import 'babel-polyfill'
import $ from 							'jquery'
import whatInput from 			'what-input'
import circles from 				'./lib/circles'
import createHTML from 			'./lib/create-html'
import inView from 					'./lib/in-view'
import slickSliders from 		'./lib/sliders'
import scrollNav from 			'./lib/scroll-nav'
import smoothScroll from 		'./lib/smooth-scroll'
import stickyNav from 			'./lib/sticky-nav'
import typeForms from 			'./lib/typeforms'
import vobToggle from 			'./lib/vob-toggle'
import reCaptcha from 			'./lib/recaptcha'

// Title Case
//import titleCase from './lib/title-case'


window.$ = $;

import Foundation from 'foundation-sites'


$(document).foundation();

// Title Case
	//titleCase()


// Scroll Nav
	scrollNav()

// VOB toggle
	vobToggle()

// Recaptcha
	reCaptcha()

// Circles
  circles("[data-circle]")

// Sticky Nav
  stickyNav("#nav")

// SmoothScroll
  smoothScroll("#nav")
  

// Sliders
  slickSliders()


// Typeform
	if( document.body.className.match('home') ) {
		// console.log("home")
		typeForms()
	} else {
		// console.log("not-home")
	}


// inView
  inView("[data-in-view]")


// Defer Youtube Embeds
	function init() {
		var vidDefer = document.getElementsByTagName('iframe');
		for (var i=0; i<vidDefer.length; i++) {
			if(vidDefer[i].getAttribute('data-src')) {
				vidDefer[i].setAttribute('src',vidDefer[i].getAttribute('data-src'));
			} 
		}
	}/// init()
	window.onload = init;



