import $ from 'jquery'
import debounce from 'lodash.debounce'
import circleProgress from 'jquery-circle-progress'


const initCircle = ($el, circleInit, circleValue, colorValue ) => {
	const windowHeight = $(window).height()
	const elementY = $el.offset().top

	if (circleInit === "true") {} 
		else {
			if ((windowHeight + window.scrollY) >= elementY) {

				$el.circleProgress({
					value: circleValue,
					startAngle: -Math.PI / 4 * 2,
					thickness: 15,
			    size: 168,
			    reverse: true,
			    fill: {color: colorValue},
			    emptyFill: "rgba(0, 0, 0, 0)"
				})

		  	$el.on('circle-animation-progress', function(event, progress, stepValue) {
		    	$(this).find('strong').html(Math.round(stepValue.toFixed(2).substr(1) * 100) + '%')
		  	})

		  	$el.on('circle-animation-end', function(event) {
		    	$(this).data("circle-init", "true")
		  	})
			}
	}

	
}///addAnimation



const elementIterator = (index, item) => {
	const $item = $(item)
	const circleValue = $item.data("circle")
	const colorValue = $item.data("circle-fill")
	const circleInit = $item.data("circle-init")
	initCircle($item, circleInit, circleValue, colorValue)

}///elementIterator


const circles = (selector) => {
	$(document).ready(()=>{
		const $el = $(selector)
		$el.each(elementIterator)
		$(window).on("scroll", debounce(e =>{
			$el.each(elementIterator)
		}, 100, { 'leading': true }))	
	})
}
	
export default circles