import $ from 'jquery'

const setVisitor = () => {
	const visitor = localStorage.setItem('visitor', 'old')
}

const noAnimate = () => {
	const $elements = $("[data-once]")
	$elements.each((index, element) => {
		$(element).removeClass("animated")
		$(element).addClass("not-animated")	
	})
	$(".billboard").removeClass("animated")
}

const newVisitor = () => {
	$(document).ready(() => {
		const viewer = localStorage.getItem("visitor")
		if(viewer === 'old') {
			noAnimate()
		}
		setVisitor()
	})///document.ready	
}///newVisitor

	
export default newVisitor