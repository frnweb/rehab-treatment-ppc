import $ from 'jquery'
import 'slick-carousel'


const slickSliders = () => {
  
  // Slider Calls
  $(document).ready(()=>{

    // Locations Slider
    $('#locations-slick').slick({
      prevArrow: `<button class="slick-prev"></button>`,
      nextArrow: `<button class="slick-next"></button>`,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      centerPadding: null,
      dots: true,
      responsive: [{
          breakpoint: 767,
          settings: 'unslick'
        }],
      mobileFirst: true
    })
    

    // Main Carousel
    $('#carousel-slick').slick({
      prevArrow: `<button class="slick-prev"></button>`,
      nextArrow: `<button class="slick-next"></button>`,
      mobileFirst: false,
      centerMode: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: false,
      centerPadding: '260px',
      responsive: [{
      // xLarge
        breakpoint: 1399,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerPadding: '260px'
        }
      }, {
      // Large
        breakpoint: 1199,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerPadding: '166px'
        }
      }, {
      // Medium
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          centerPadding: '110px'
        }
      }, {
      // Small
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerPadding: '0px'
        }
      }]
    })


    // Testimonial Slider
    $('#testimonials-slick').slick({
      prevArrow: `<button class="slick-prev"></button>`,
      nextArrow: `<button class="slick-next"></button>`,
      mobileFirst: true,
      centerMode: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      centerPadding: null,
      dots: false,
      responsive: [{
          breakpoint: 1199,
          settings: {
            centerPadding: '10px',
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true
          }
        }, {
          breakpoint: 991,
          settings: {
            centerPadding: '0px',
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true
          }
        }, {
          breakpoint: 767,
          settings: {
            centerPadding: '15px',
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true
          }
        }]
    })


    // Facility Carousel
    $('#ourfacility-slick').slick({
      prevArrow: `<button class="slick-prev"></button>`,
      nextArrow: `<button class="slick-next"></button>`,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      centerPadding: '0px',
      responsive: [{
            breakpoint: 991,
            settings: {
              centerPadding: null,
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }, {
            breakpoint: 767,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              centerPadding: null
            }
          }]
    })


  });


}///slickSliders

export default slickSliders



  





