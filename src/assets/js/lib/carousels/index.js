import $ from 'jquery'
import 'slick-carousel'

export default (selector, options) => {
  const defaultOptions = {
    prevArrow: `<button class="slick-prev"></button>`,
    nextArrow: `<button class="slick-next"></button>`,
    centerMode: true,
    infinite: true,
    autoplaySpeed: 5000,
    centerPadding: '300px',
  }

  const slickOptions = Object.assign({}, defaultOptions, options)

  $(selector).slick(slickOptions)
}
