import $ from 'jquery'
import Foundation from 'foundation-sites'

export default selector => {
  $(document).ready(() => {
    $(selector)
      .find('a.link--nav')
      .each((index, item) => {
        const $el = $(item)
        const shouldSmoothScroll = /^#.+?$/.test($el.attr('href'))

        if (shouldSmoothScroll) {
          new Foundation.SmoothScroll($el, {
            animationDuration: 700,
            animationEasing: 'swing',
            threshold: 50,
            offset: 85,
          })
        }
      })
  })
}


