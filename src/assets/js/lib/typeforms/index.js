import $ from 'jquery'
import * as typeformEmbed from '@typeform/embed'


const typeForms = () => {
    const popup1 = typeformEmbed.makePopup('https://foundationsrecoverynetwork.typeform.com/to/Vy86FL', {
      mode: 'popup',
      hideHeaders: true,
      hideFooters: true,
      onSubmit: function () {
        console.log('Typeform successfully submitted')
      }
    })
    const popup2 = typeformEmbed.makePopup('https://foundationsrecoverynetwork.typeform.com/to/bPqKFX', {
      mode: 'popup',
      hideHeaders: true,
      hideFooters: true,
      onSubmit: function () {
        console.log('Typeform successfully submitted')
      }
    })
    document.getElementById('assessment-form-1').addEventListener('click', function () {
      popup1.open();
    });
    document.getElementById('assessment-form-2').addEventListener('click', function () {
      popup2.open();
    });
}///slickSliders
export default typeForms