import $ from 'jquery'

const scrollNav = () => {
	$(document).ready(() =>{

		$(window).on("scroll", function(e){
			const scrollY = $(window).scrollTop()
			const scrollnav = $("#scroll-nav")

			if(scrollY >= 105) {
				scrollnav.addClass("show")
			} else {
				scrollnav.removeClass("show")
			}
		})

	})
}

	
export default scrollNav