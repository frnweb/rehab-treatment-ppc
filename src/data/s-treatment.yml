# Page Info
title: &siteTitle Our Treatment Model
id: 'locations'

# Header
header:
  layout: 'facilities'
  top-bar:
    left:
      text: Logo
      logo: 'assets/img/oakslogo.svg'
    right:
      button: "phone"
      href: "tel:888-782-4943"
      text: "888-782-4943"
      ga-category: "Phone Numbers"
      ga-action: "Phone Clicks"
      ga-label: "Phone Clicks Sitelink-Navigation"


# Sections
sections:
  
# Intro
  intro:
    h2: "Treatment"


# The Foundations Treatment Model
  model:
    id: "treatment--model"
    card:
      class: "left"
      index: "0"
      paragraph: "The Foundations Treatment Model provides the highest quality treatment for substance use and mental health disorders at each of our facilities across the United States. We use both traditional and cutting-edge therapeutic methods to treat the whole person – mind, body and spirit.
        <br><br>
        Our holistic, fully integrated treatment model identifies and addresses underlying mental health issues that may contribute to substance use disorders and impair successful and sustained recovery. We see each client as an individual with specific needs, a unique personal history and their own relationship to treatment and recovery. During the admissions process, we conduct a rigorous clinical assessment to ensure all treatment programs are personalized to meet the needs of each person and have the greatest chance of success."

    media:
      type: "image"
      src: "assets/img/sitelink/treatment-model.jpg"
      video-src: "" 


# Treatment Breakdown
  breakdown:
    id: "treatment--breakdown"
    h2: "Our treatment services include but are not limited to:"
    big-list:
      -
        list:
          - item: "Residential care"
          - item: "Outpatient services"
          - item: "Partial hospitalization programs"
          - item: "Medically supervised detox"
          - item: "Physician-supervised medication management"
          - item: "Individual therapy, including:"
            accordion: true
            list:
              - item: "Eye Movement Desensitization and Reprocessing (EMDR)"
              - item: "Cognitive Behavioral Therapy (CBT)"
              - item: "Dialectical Behavioral Therapy (DBT)"
              - item: "Cognitive Processing Therapy (CPT)"

          - item: "Group Therapy"
          - item: "Family therapy"
      -
        list:
          - item: "Complementary therapies, including:"
            accordion: true
            list:
              - item: "Yoga"
              - item: "Art"
              - item: "Music"
              - item: "Equine therapy"
              - item: "Meditation"
              - item: "Mindfulness"
              - item: "Adventure therapy"
              - item: "Experiential therapy"
              - item: "Hiking"

          - item: "Diverse educational groups, including:"
            accordion: true
            list:
              - item: "Trauma resolution"
              - item: "Boundary setting"
              - item: "Understanding codependency"
              - item: "Anger management"
              - item: "Mood management"
              - item: "Stress management"
              - item: "Grief and loss processing"

          - item: "Motivational Enhancement Therapy"
          - item: "Relapse prevention"
          - item: "Alumni support in our Life Challenge program"

      
# State-of-the-Art Facilities
  facilities:
    id: "treatment--facilities"
    card:
      class: "left"
      index: "0"
      h4: "State-of-the-Art Facilities"
      subhead: "Depending on the location, you’ll find:"
      line: "true"
      list:
        -
          item: "Delicious, nutritious meals prepared on-site by executive gourmet chefs"
        -
          item: "Basketball, volleyball and tennis courts"
        -
          item: "Ropes courses"
        -
          item: "Weight rooms"
        -
          item: "Hiking trails"
        -
          item: "Massage rooms"
        -
          item: "Acupuncture rooms"
        -
          item: "Yoga rooms"
        -
          item: "Meditation rooms"
        -
          item: "Equine facilities"
        -
          item: "Meditation and lounge areas"
        -
          item: "Swimming pools"
        -
          item: "Hot tubs"
        -
          item: "Spas"

    media:
      type: "image"
      src: "assets/img/sitelink/treatment-facilities.jpg"



# Integrated Treatment
  integrated:
    id: "treatment--integrated"
    class: "highlights"
    intro:
      id: "treatment--integrated-intro"
      class: "show-for-medium"
      card:
        index: "0"
        h2: "What Is Integrated Treatment?"
        paragraph: "We understand that since no two individuals are exactly the same, no two individuals experience substance use or co-occurring mental health disorders in exactly the same way. Therefore, we create a customized treatment and recovery plan for each client we meet."
    cards:
      - 
        h3: "Comprehensive Assessments"
        paragraph: "First, we perform comprehensive psychiatric evaluations and addiction assessments. We make sure we have a complete history for every client and work to understand their issues in context. A proper diagnosis – which some of our clients have never received – is a critical first step in creating a viable recovery plan. All the circumstances surrounding their physical and emotional history can contribute to their current state of need. We leave no stone unturned, and ensure we have all information necessary to assist individuals on their journey to health, wellness and sustainable recovery."
        cta: "Ask us about our residential programs"
      - 
        h3: "Motivational Interviewing"
        paragraph: "We employ a technique called Motivational Interviewing that enables our patients to become a full partner on their treatment and recovery journey. By focusing on the goals, hopes, needs and wants of the individual, we inspire them to move forward. Motivational interviewing allows them to track their progress using a Stages-of-Change approach. That way they don’t have to rely on us to tell them where in their journey they are: they see it for themselves."
        cta: "Ask us about our residential programs"
      - 
        h3: "Patient-Centered Care"
        paragraph: "We’re fully dedicated to the personal needs, goals and long-term success of each individual patient. Healing is the core of everything we do, from our premier integrated treatment approach right down to every detail of our services. Our restorative environments, our creative, engaging activities and our comprehensive therapeutic programs all have one goal: healing every client. In 2011, FRN received the James W. West Quality Award from the National Association of Addiction Treatment Providers for our patient-centered care initiative."
        cta: "Ask us about our residential programs"
      - 
        h3: "Accreditation"
        paragraph: "Our treatment centers follow current best practices for the care and well-being of our patients. Each facility is accredited by either CARF or Joint Commission, and all facilities are certified by LegitScript, which demonstrates a commitment to transparency and ethical practices. All facilities are also full members of the National Association of Addiction Treatment Providers (NAATP)."
        cta: "Ask us about our residential programs"


# What To Expect (Next Step)
  whattoexpect:
    id: "whattoexpect"
    card:
      index: "1"
      h2: "What’s the next step?"
      line: "true"
      paragraph: "The next step is simple: pick up the phone and give us a call. Our professional, kind and compassionate admissions coordinators are ready to listen, right now. They’ll perform an initial assessment over the phone to help you decide which treatment program is a match for your needs, whether it’s at one of our facilities or not. They’ll also help you verify your insurance benefits and answer any other questions you may have. We’re here to guide you through the process and make sure you’re making the most informed decision possible."


# Footer
  footer:
    card:
      index: "0"
      h3: "Take Your Life Back Today We’re Here to Help"